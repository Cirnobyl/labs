package com.company;

public class Main {

    public static void main(String[] args) {
        /* This is the 'main' function, the entry point into your Java Program. By default,
            most Java programs will have a main method.


           The following code will run, but will not give you the correct output. At the bottom of the screen is an
           output console. You won't be touching anything here for now. On the sidebar to your left, double-click
           into the CardfightCard class.
       */

        //By the way, these lines are called "comments". '//' will mark out a single line

        /* while
            the
            slash+star combo
            will
            comment
            out
            multiple
            lines
            at
            once
         */

        //Comments are skipped over by the compiler - they'll be ignored, so feel free to put anything you want in them.
        //Typically they're used for clarification and or reminders like TODOs
        //Remember that source code is HUMAN READABLE!

        //Setup
        TestHarness testHarness= new TestHarness();
        CardfightCard myCard = new CardfightCard();
        int ex1power = 4000;
        int ex2power = 7000;
        int expectedPowerEx1 = 11000;
        int expectedPowerEx2 = 18000;
        int expectedPowerEx3 = 11000;

        /*Lab 1 - Working with setters and getters */


        //Ex1
        //Run
        myCard.setPower(11000);

        //Verify
        System.out.println("My card's power is " + myCard.getPower());
        testHarness.verify(expectedPowerEx1, myCard.getPower());

        //Ex2
        //Run
        myCard.addPower(7000);

        //Verify
        System.out.println("My card's power is " + myCard.getPower());
        testHarness.verify(expectedPowerEx2, myCard.getPower());

        //Ex3
        //Run
        myCard.subtractPower(/*7000*/); //uncomment the 7000 by removing the /* and */

        //Verify
        System.out.println("My card's power is " + myCard.getPower());
        testHarness.verify(expectedPowerEx3, myCard.getPower());
    }
}
