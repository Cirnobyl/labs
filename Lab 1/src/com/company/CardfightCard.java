package com.company;

/**
 * Created by Shameimaru on 7/16/2017.
 */
public class CardfightCard {
    /* Hello! Welcome to the CardfightCard class.
        Below are the fields or variables that describe this class.
        Take a moment to read them over.
     */

    private String cardName = ""; // The doublequote is read as 'empty string'. It is literally empty - it's not even a space.
    private int power = 0;
    private int grade = 0;
    private boolean isStand = true;



/*
    Arguments are passed into functions so that the function may act on them.
    In math class, you probably saw the form, f(x). 'f' is your function, and x is your argument.

    You hold argument you want through parameter variables.

    Arguments are the actual value you're passing in, Parameters are the variable used to hold the argument.
    Example - the variable 'p' is your PARAMETER.

    public void addPower(int p){
        ...
    }

    When I call myCard.addPower(1000); I'm passing in the ARGUMENT 1000.
*/

    /*
        I've created this method for you. It's a method that takes in an integer argument, and sets the CardfightCard
        object instance's power to be whatever the parameter is holding.

        Its return type is void, since we don't expect it to give back any value.
  */

    public void setPower(int p){
        this.power = p;
    }

    public int getPower(){
        //This does not return the proper value.
        //Change this so that it returns the CardfightCard's power.
        return 0;
    }
    /*

     */
    public void addPower(int p){
        System.out.println("Added " + p + " power!");
        //TODO add in the proper behavior for this method
    }

    public void subtractPower(){ //TODO add in the proper parameters for this method
        //TODO add in the proper behavior for this method
    }

    public void getIsStand(){ //TODO change the return type for this method
        //TODO add in the proper behavior for this method
    }

    public void getGrade(){ //TODO change the return type for this method
        //TODO add in the proper behavior for this method
    }


    public String getCardName(){ //TODO change the return type for this method
        //TODO add in the proper behavior for this method
        return null;
    }
}
