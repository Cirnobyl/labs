package com.company;

/**
 * Created by Shameimaru on 7/16/2017.
 */
public class TestHarness {
    public void verify(Object expected, Object actual){
        if(expected == actual){
            System.out.println("I expected " + expected.toString()+ ". I got " + actual.toString() + ". PASS");
        } else {
            System.out.println("I expected " + expected.toString()+ ". I got " + actual.toString() + ". FAIL");
        }
    }
}
